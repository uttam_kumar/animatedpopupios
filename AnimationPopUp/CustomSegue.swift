//
//  CustomSegue.swift
//  AnimationPopUp
//
//  Created by Syncrhonous on 15/4/19.
//

import UIKit

class CustomSegue: UIStoryboardSegue {
    
    override func perform() {
        scale()
    }
    
    
    func scale(){
        let toVIewController = self.destination
        let fromViewController = self.source
        
        let containerView = fromViewController.view.superview
        let originCenter = fromViewController.view.center
        toVIewController.view.transform = CGAffineTransform(scaleX: 0.05, y: 0.05)
        toVIewController.view.center = originCenter
        
        containerView?.addSubview(toVIewController.view)
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            toVIewController.view.transform = CGAffineTransform.identity
        }, completion: { success in
            //fromViewController.present(toVIewController,animated: false, completion: nil)
        })
    }

}

class UIUnwindCustomSegue: UIStoryboardSegue {
    
    override func perform() {
        scale()
    }
    
    
    func scale(){
        let toVIewController = self.destination
        let fromViewController = self.source
        
        fromViewController.view.superview?.insertSubview(toVIewController.view, at: 0)
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            fromViewController.view.transform = CGAffineTransform(translationX: 0.05, y: 0.05)
        }, completion: { success in
            fromViewController.dismiss(animated: false, completion: nil)
            //toVIewController.dismiss(animated: false, completion: nil)
        })
    }
    
}
