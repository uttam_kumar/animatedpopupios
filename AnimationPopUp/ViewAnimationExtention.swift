//
//  ViewAnimationExtention.swift
//  AnimationPopUp
//
//  Created by Syncrhonous on 16/4/19.
//

import Foundation
import UIKit
extension UIView {
    
    func viewPulsate() {
        
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 1
        pulse.fromValue = 0.0
        pulse.toValue = 1.0
        //pulse.autoreverses = true
        //pulse.repeatCount = 1
        //pulse.initialVelocity = 0.1
        //pulse.damping = 0.5
        
        layer.add(pulse, forKey: "pulse")
    }
    
    func viewPulsate2() {
        
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 1.2
        pulse.fromValue = 1.0
        pulse.toValue = 0.0
        //pulse.autoreverses = true
        //pulse.repeatCount = 1
        //pulse.initialVelocity = 0.1
        //pulse.damping = 0.5
        
        layer.add(pulse, forKey: "pulse")
    }
    
    func viewFlash() {
        
        let flash = CABasicAnimation(keyPath: "transform.scale")
        flash.duration = 0.65
        flash.fromValue = 0
        flash.toValue = 1.0
        flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        //flash.autoreverses = true
        //flash.repeatCount = 1
        
        layer.add(flash, forKey: nil)
    }
    
    
    func viewFlash2() {
        
        let flash = CABasicAnimation(keyPath: "transform.scale")
        flash.duration = 1.1
        flash.fromValue = 1
        flash.toValue = 0.0
        flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        //flash.autoreverses = true
        //flash.repeatCount = 1
        
        layer.add(flash, forKey: nil)
    }
    
    
    func viewShake() {
        
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.1
        shake.repeatCount = 2
        shake.autoreverses = true
        
        let fromPoint = CGPoint(x: center.x - 5, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)
        
        let toPoint = CGPoint(x: center.x + 5, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)
        
        shake.fromValue = fromValue
        shake.toValue = toValue
        
        layer.add(shake, forKey: "position")
    }
}
