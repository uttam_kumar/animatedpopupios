//
//  ViewController.swift
//  AnimationPopUp
//
//  Created by Syncrhonous on 15/4/19.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var viewg: UIView!
    
    @IBOutlet weak var buttons: UIButton!
    
    var pressesInOneSecond = 1
    var timeStart: NSDate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue){
        
    }
    
    
    
    override func unwind(for unwindSegue: UIStoryboardSegue, towards subsequentVC: UIViewController) {
        let segue = UIUnwindCustomSegue(identifier: unwindSegue.identifier, source: unwindSegue.source, destination: unwindSegue.destination)

        segue.perform()
    }
    
    
    
    
    // Example of using the extension on button press
    @IBAction func pulseButtonPressed(_ sender: UIButton) {
        
        if viewg.isHidden {
            viewg.isHidden = false
            //viewg.viewPulsate()
            viewg.viewFlash()
        }else{
            //viewg.viewPulsate2()
            viewg.viewFlash2()
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                self.viewg.isHidden = true
                print("dfhhrtrr")
            }
        }
        
    }
    
    @IBAction func flashButtonPressed(_ sender: UIButton) {
        sender.flash()
    }
    
    @IBAction func shakeButtonPressed(_ sender: UIButton) {
        sender.shake()
        if timeStart == nil {
            timeStart = NSDate()
        } else {
            let end = NSDate()
            let timeInterval: Double = end.timeIntervalSince(timeStart! as Date)
            if timeInterval == 1{
                print("hello 1")
            }else if timeInterval < 1 {
                pressesInOneSecond = pressesInOneSecond + 1
            } else {
                // One second has elapsed and the button press count
                // is now stored in pressesInOneSecond.
                print("presses in one second was \(pressesInOneSecond)")
                pressesInOneSecond = 1
                timeStart = nil
            }
        }
    }

    @IBAction func buttonClose(_ sender: UIButton) {

    }
    

}

